resource "aws_launch_configuration" "bibit-launchconfig" {
name_prefix = "bibit-launchconfig"
image_id = "ami-090717c950a5c34d3"
instance_type = "t2.micro"
key_name = aws_key_pair.mykeypair.key_name
security_groups = [aws_security_group.allow-ssh.id]
}
resource "aws_autoscaling_group" "bibit-autoscaling" {
name = "bibit-autoscaling"
vpc_zone_identifier = [aws_subnet.main-private-1.id]
launch_configuration = aws_launch_configuration.bibit-launchconfig.name
min_size = 2
max_size = 5
health_check_grace_period = 300
health_check_type = "EC2"
force_delete = true
tag {
key = "Name"
value = "ec2 instance"
propagate_at_launch = true
}
}