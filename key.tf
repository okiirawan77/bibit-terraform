# Create a key pair by importing the contents of the SSH
# public key from the "mykeypair.pub" file
resource "aws_key_pair" "mykeypair" {
  key_name   = "id_rsa"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}

# Show the key pair name as a result on the screen
output "keypair_name" {
  value = aws_key_pair.mykeypair.key_name
}
