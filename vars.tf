variable "PATH_TO_PRIVATE_KEY" {
default = "id_rsa"
}
variable "PATH_TO_PUBLIC_KEY" {
default = "id_rsa.pub"
}
variable "AMIS" {
type = string
default = "ami-090717c950a5c34d3"
}